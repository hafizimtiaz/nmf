function [W, H, R, iter, elapse, HIS] = nmf_opgd(V, Winit, Hinit, Rinit, lambda, tol, maxiter)

% Online PGD algorithm
% Reference
% Zhao, Renbo; Tan, Vincent Y. F., 2016. 
% Online Nonnegative Matrix Factorization With Outliers 
% IEEE Transactions on Signal Processing, vol. 65, issue 3, pp. 555-570 


% W , H , R = output solution
% iter = required iterations for solving
% elapse : CPU time in seconds.
% HIS : history of computation,
% 	niter - total iteration number spent for each sub-iteration
%   cpus - CPU seconds at iteration rounds,
%   objf - objective function values at iteration rounds,
%   prjg - projected gradient norm at iteration rounds.
%   relerr - relative error at each top-level iteration

% V = data matrix, samples in columns
% lambda = penalty parameter for outlier regularization
% Winit , Hinit, Rinit = initial solution
% tol = tolerance for a relative stopping condition (section 7-B)
% maxiter = limit of iterations

[D,N] = size(V);
[~, K] = size(Winit);
M = 1;
A = zeros(K,K);
B = zeros(D,K);
W = Winit;
H = Hinit;
R = Rinit;

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = (1/2) * trace(V'*V + R'*R + (W*H)'*(W*H)) - trace(R'*V + V'*W*H - R'*W*H) + lambda * sum(sum(abs(R)));
HIS.relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');
elapse = cputime;

for n = 1:N
    v = V(:,n);
    [ht, rt, iterhr] = pgd_hr(v, W, lambda, M, tol, min(200,maxiter));
    H(:,n) = ht;
    R(:,n) = rt;
    
    A = (1/n) * ((n-1) * A + ht * ht');
    B = (1/n) * ((n-1) * B + (v - rt) * ht');
    [W, iterw] = pgd_W(W, A, B, tol, min(200,maxiter));
        
    HIS.niter = HIS.niter + iterhr + iterw;
    
    % Output running detials
    HIS.cpus = [HIS.cpus, cputime-elapse];
    objf = (1/2) * trace(V'*V + R'*R + (W*H)'*(W*H)) - trace(R'*V + V'*W*H - R'*W*H) + lambda * sum(sum(abs(R)));
    HIS.objf = [HIS.objf, objf];
    relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');
    HIS.relerr = [HIS.relerr,relerr];
    
    if rem(n,10) == 0
        fprintf('%d:\tobjective value = %f.\n', n,HIS.objf(end));
    end
end
iter = HIS.niter;

elapse = cputime - elapse;

fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);

% helper functions
function [h,r,num_iter] = pgd_hr(v, W, lambda, M, tol, maxiter)
iter = 0;
flag = true;
[D,K] = size(W);
h = rand(K,1);
r = rand(D,1);
const = 0.7;
objective_old = (1/2) * norm(v - W * h - r)^2 + lambda * norm(r,1);

while (iter <= maxiter) && flag
    iter = iter + 1;
    
    % computing new h
    L = norm(W)^2;
    eta = const / L;
    grad_h = (W' * W) * h + W' * (r - v);
    h = h - eta * grad_h;
    h(h < 0) = 0;
    
    % computing new r
    tmp = v - W * h;
    for d = 1:D
        if abs(tmp(d)) < lambda
            r(d) = 0;
        elseif (abs(tmp(d)) >= lambda) && (abs(tmp(d)) <= lambda + M)
            r(d) = tmp(d) - sign(tmp(d)) * lambda;
        else
            r(d) = sign(tmp(d)) * M;
        end
    end
    
    % stopping condition check
    objective_new = (1/2) * norm(v - W * h - r)^2 + lambda * norm(r,1);
    if abs(objective_new - objective_old) < tol * objective_old
        flag = false;
    else
        objective_old = objective_new;
    end
%     if rem(iter,10) == 0
%       fprintf('.');
%     end
end
num_iter = iter;

function [W, num_iter] = pgd_W(Winit, A, B, tol, maxiter)
[~,K] = size(Winit);
W = Winit;
objective_old = (1/2) * trace((W'*W) * A) - trace(W' * B);
iter = 0;
flag = true;
const = 0.7;

while (iter <= maxiter) && flag
    iter = iter + 1;
    
    L = norm(W)^2;
    eta = const / L;
    grad_W = W * A - B;
    tmp = W - eta * grad_W;
    for k = 1:K
        num = tmp(:,k);
        num(num < 0) = 0;
        num = num / max(1,norm(num));
        W(:,k) = num;
    end
    
    % stopping condition check
    objective_new = (1/2) * trace((W'*W) * A) - trace(W' * B);
    if abs(objective_new - objective_old) < tol * objective_old
        flag = false;
    else
        objective_old = objective_new;
    end
%     if rem(iter,10) == 0
%       fprintf('*');
%     end
end
num_iter = iter;

function y = mySoftThresh(x, lambda, M)
    % Implementation of the soft thresholding function defined in Renbo
    % Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    
    % x - input vector of dimension F
    % lambda - penalty parameter (here threshold parameter)
    % M - parameter of the constraint set of the outlier r
    % y - output vector of dimension F
    
    y = 0 * (abs(x) < lambda) + (x - sign(x) * lambda) .* ((x >= lambda) & (x <= M)) + (sign(x) * M) .* (x >= lambda + M);
return