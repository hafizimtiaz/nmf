function [W, H, R, iter, elapse, HIS] = nmf_bpgd(V, Winit, Hinit, Rinit, lambda, tol, maxiter)

% Batch PGD algorithm
% Reference
% Zhao, Renbo; Tan, Vincent Y. F., 2016. 
% Online Nonnegative Matrix Factorization With Outliers 
% IEEE Transactions on Signal Processing, vol. 65, issue 3, pp. 555-570 


% W , H , R = output solution
% iter = required iterations for solving
% elapse : CPU time in seconds.
% HIS : history of computation,
% 	niter - total iteration number spent for each sub-iteration
%   cpus - CPU seconds at iteration rounds,
%   objf - objective function values at iteration rounds,
%   prjg - projected gradient norm at iteration rounds.
%   relerr - relative error at each top-level iteration

% V = data matrix, samples in columns
% lambda = penalty parameter for outlier regularization
% Winit , Hinit, Rinit = initial solution
% tol = tolerance for a relative stopping condition (section 7-B)
% maxiter = limit of iterations

[~, K] = size(Winit);
M = 1;
W = Winit;
H = Hinit;
R = Rinit;
objective_old = (1/2)*norm(V - W*H - R, 'fro') + lambda*sum(sum(abs(R)));
flag = true;
iter = 0;

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = (1/2) * norm(V - W*H - R,'fro')^2 + lambda * sum(sum(abs(R)));
HIS.relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');
elapse = cputime;

while (iter <= maxiter) && flag
    iter = iter + 1;
    eta_H = 0.7/norm(W)^2;
    H = max(0,H - eta_H * W' * (W*H + R - V));
    
    R = mySoftThresh(V - W*H, lambda, M);
    
    eta_W = 0.7/norm(H*H', 'fro');
    grad_W = W * (H*H') - (V-R)*H';
    W = max(0,W - eta_W * grad_W);
    for k = 1:K
      W(:,k) = W(:,k) / max( 1 , norm(W(:,k)) );
    end
    
    % stopping condition check
    objective_new = (1/2)*norm(V - W*H - R, 'fro') + lambda*sum(sum(abs(R)));
    if abs(objective_new - objective_old) < tol * objective_old
        flag = false;
    else
        objective_old = objective_new;
    end
    
    % Output running detials
    objf = (1/2) * norm(V - W*H - R,'fro')^2 + lambda * sum(sum(abs(R)));
    HIS.objf = [HIS.objf, objf];
    HIS.cpus = [HIS.cpus, cputime-elapse];
    relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');
    HIS.relerr = [HIS.relerr,relerr];
    
    if rem(iter,10) == 0
        fprintf('%d:\tobjective value = %f.\n', iter,HIS.objf(end));
    end
end
HIS.iter = iter;
    
elapse = cputime - elapse;

fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);

% helper functions
function y = mySoftThresh(x, lambda, M)
    % Implementation of the soft thresholding function defined in Renbo
    % Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    
    % x - input vector of dimension F
    % lambda - penalty parameter (here threshold parameter)
    % M - parameter of the constraint set of the outlier r
    % y - output vector of dimension F
    
    y = 0 * (abs(x) < lambda) + (x - sign(x) * lambda) .* ((x >= lambda) & (x <= M)) + (sign(x) * M) .* (x >= lambda + M);
return