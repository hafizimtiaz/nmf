function [W,H,R,iter,elapse,HIS] = nmf_myneogm(V,W_INIT,H_INIT,R_INIT,LAMBDA,TOL,MAX_ITER)

% Code for non-negative Matrix Factorization with outliers 

% Reference
%  N. Guan, D. Tao, Z. Luo, and B. Yuan, "NeNMF: An Optimal Gradient Method
%  for Non-negative Matrix Factorization", IEEE Transactions on Signal
%  Processing, Vol. 60, No. 6, PP. 2882-2898, Jun. 2012. (DOI:
%  10.1109/TSP.2012.2190406)

% Model: V = WH + R
% V (d x n): data matrix including n samples in d-dimensions
% W (d x k): basis matrix including k bases in d-dimensions
% H (k x n): coefficients matrix includeing n encodings in k-dimensions
% R (d x n): outlier matrix includeing n outlier vectors in d-dimensions

% Written by Hafiz Imtiaz (hafiz.imtiaz@rutgers.edu), based on the NeNMF code by Naiyang Guan

% <Inputs>
%        V      : input data matrix (d x n)
%        W_INIT : (d x k) initial value for W.
%        H_INIT : (k x n) initial value for H.
%        R_INIT : (d x n) initial value for R.
%        LAMBDA : regularizer for outliers.
%        TOL    : stopping tolerance.
%        MAX_ITER : maximum number of iterations.

% <Outputs>
%        W : estimated basis matrix (d x k).
%        H : estimated coefficients matrix (k x n).
%        R : estimated outlier matrix (d x n).
%        iter : number of top-level iterations.
%        elapse : CPU time in seconds.
%        HIS : history of computation,
%               niter - total iteration number spent for each sub-iteration
%               cpus - CPU seconds at iteration rounds,
%               objf - objective function values at iteration rounds,
%               prjg - projected gradient norm at iteration rounds.
%               relerr - relative error at each top-level iteration


% Default setting
MaxIter = MAX_ITER;
MinIter = 10;
MaxTime = 100000;
W0 = W_INIT;
H0 = H_INIT;
R0 = R_INIT;
tol = TOL;
M = 1; % bound on outliers

ITER_MAX = 1000;      % maximum inner iteration number for sub-iteration
ITER_MIN = 10;        % minimum inner iteration number for sub-iteration

% Initialization
W = W0;
H = H0;
R = R0;
HVt = H*(V-R)';
HHt = H*H';
WtV = W'*(V-R);
WtW = W'*W;
GradW = W * HHt - HVt';
GradH = WtW * H - WtV;
GradR = R - (V - W*H) + LAMBDA*sign(R);

init_delta = sqrt(norm(GradW,'fro')^2 + norm(GradH,'fro')^2 + norm(GradR,'fro')^2);
tolH = max(tol,1e-3)*init_delta;
tolW = tolH;               % Stopping tolerance

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = (1/2) * trace((V - W*H - R)'*(V - W*H - R)) + LAMBDA * sum(sum(abs(R)));
HIS.prjg = init_delta;
HIS.relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');

% Iterative updating
elapse = cputime;
W = W';
for iter = 1:MaxIter
    
    % Optimize H and R with W fixed
    [H,R,iterH] = myNNLS(V,W',H,R,LAMBDA,ITER_MIN,ITER_MAX,tol);
    if iterH < ITER_MIN
        tol = tol/10;
    end
    HHt = H*H';
    HVt = H*(V-R)';
    
    % Optimize W with H and R fixed
    [W,iterW,GradW] = NNLS(W,HHt,HVt,ITER_MIN,ITER_MAX,tolW);
    if iterW < ITER_MIN
        tolW = tolW/10;
    end
    WtW = W*W';
    WtV = W*(V-R);
    GradH = WtW * H - WtV;
    GradR = R - (V - W'*H) + LAMBDA*sign(R);
    
   
    HIS.niter = HIS.niter + iterH + iterW;
    HIS.cpus = [HIS.cpus, cputime-elapse];

    delta = sqrt(  norm(myProjGrad(H, GradH, 0, 1e10), 'fro')^2 + ...
                   norm(myProjGrad(W, GradW, 0, 1e10), 'fro')^2 + ...
                   norm(myProjGrad(R, GradR, -M, M), 'fro')^2);
    
    % Output running detials
    objf = (1/2) * trace((V - W'*H - R)'*(V - W'*H - R)) + LAMBDA * sum(sum(abs(R)));
    HIS.objf = [HIS.objf, objf];
    HIS.prjg = [HIS.prjg, delta];
    relerr = norm(V - W'*H - R,'fro') / norm(V, 'fro');
    HIS.relerr = [HIS.relerr,relerr];
    
    if rem(iter,10) == 0
        fprintf('%d:\tstopping criteria = %e,\tobjective value = %f.\n', iter,delta/init_delta,HIS.objf(end));
    end
    
    % Stopping condition
    if (delta <= tol*init_delta && iter >= MinIter) || HIS.cpus(end) >= MaxTime
        break;
    end
end
W = W';
elapse = cputime - elapse;

fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);
return;

% Helper functions
function [H,iter,Grad] = NNLS(Z,WtW,WtV,iterMin,iterMax,tol)

if ~issparse(WtW)
    L = norm(WtW);	% Lipschitz constant
else
    L = norm(full(WtW));
end
H = Z;    % Initialization
Grad = WtW*Z - WtV;     % Gradient
alpha1 = 1;

for iter = 1:iterMax
    H0 = H;
    H = max(Z - Grad/L,0);    % Calculate sequence 'Y'
    alpha2 = 0.5*(1 + sqrt(1 + 4*alpha1^2));
    Z = H + ((alpha1-1)/alpha2)*(H-H0);
    alpha1 = alpha2;
    Grad = WtW * Z - WtV;
    
    % Stopping criteria
    if iter >= iterMin
        % Lin's stopping condition
        pgn = norm(Grad(Grad < 0 | Z > 0));
        if pgn <= tol
            break;
        end
    end
end

Grad = WtW * H - WtV;

return;

function [H,R,iter,Grad] = myNNLS(V,W,Z,R,lambda,iterMin,iterMax,tol)

WtW = W'*W;
if ~issparse(WtW)
    L = norm(WtW);	% Lipschitz constant
else
    L = norm(full(WtW));
end
H = Z;    % Initialization
WtV = W'*(V-R);
Grad = WtW*H - WtV;     % Gradient
GradR = R - (V - W*H) + lambda*sign(R);
initgrad = sqrt(norm(Grad, 'fro')^2 + norm(GradR, 'fro')^2);

alpha1 = 1; M = 1;

for iter = 1:iterMax
    % update H
    H0 = H;
    H = max(Z - Grad/L,0);    % Calculate sequence 'Y'
    alpha2 = 0.5*(1 + sqrt(1 + 4*alpha1^2));
    Z = H + ((alpha1-1)/alpha2)*(H-H0);
    alpha1 = alpha2;
    
    % update R
    R = mySoftThresh(V - W*H, lambda, 1);
    GradR = R - (V - W*H) + lambda*sign(R);
    
    WtV = W'*(V-R);
    Grad = WtW * Z - WtV;
    
    % Stopping criteria
    if iter >= iterMin
        % Lin's stopping condition
        projnorm = sqrt(  norm(myProjGrad(Z, Grad, 0, 1e10), 'fro')^2 + norm(myProjGrad(R, GradR, -M, M), 'fro')^2 );
        if (projnorm <= tol*initgrad) 
            break;
        end
    end
end

Grad = WtW * H - WtV;

return;

function pgd = myProjGrad(x, gradx, lLim, uLim)
pgd = gradx .* ((x >= lLim) & (x <= uLim)) + min(0, gradx) .* (x <= lLim) + max(0, gradx) .* (x >= uLim);

function y = mySoftThresh(x, lambda, M)
    % Implementation of the soft thresholding function defined in Renbo
    % Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    
    % x - input vector of dimension F
    % lambda - penalty parameter (here threshold parameter)
    % M - parameter of the constraint set of the outlier r
    % y - output vector of dimension F
    
    y = 0 * (abs(x) < lambda) + (x - sign(x) * lambda) .* ((x >= lambda) & (x <= M)) + (sign(x) * M) .* (x >= lambda + M);
return