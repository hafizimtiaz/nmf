function [W,H,iter,elapse,HIS] = nmf_neogm(V,W_INIT,H_INIT,TOL,MAX_ITER)

% Non-negative Matrix Factorization via Nesterov's Optimal Gradient Method.
% NeNMF: Matlab Code for Efficient NMF Solver

% Reference
%  N. Guan, D. Tao, Z. Luo, and B. Yuan, "NeNMF: An Optimal Gradient Method
%  for Non-negative Matrix Factorization", IEEE Transactions on Signal
%  Processing, Vol. 60, No. 6, PP. 2882-2898, Jun. 2012. (DOI:
%  10.1109/TSP.2012.2190406)

% The model is V \approx WH, where V, W, and H are defined as follows:
% V (m x n-dimension): data matrix including n samples in m-dimension
% space;
% W (m x r-dimension): basis matrix including r bases in m-dimension space;
% H (r x n-dimension): coefficients matrix includeing n encodings in
% r-dimension space.

% Written by Naiyang Guan (ny.guan@gmail.com)
% Copyright 2010-2012 by Naiyang Guan and Dacheng Tao
% Modified at Sept. 25 2011
% Modified at Nov. 4 2012

% <Inputs>
%        V : Input data matrix (m x n)
%        MAX_ITER : Maximum number of iterations. Default is 1,000.
%        W_INIT : (m x r) initial value for W.
%        H_INIT : (r x n) initial value for H.
%        TOL    : Stopping tolerance. Default is 1e-5. 
%        MAX_ITER : maximum number of iterations
%        If you want to obtain a more accurate solution, decrease TOL and increase MAX_ITER at the same time.

% <Outputs>
%        W : Obtained basis matrix (m x r).
%        H : Obtained coefficients matrix (r x n).
%        iter : Number of iterations.
%        elapse : CPU time in seconds.
%        HIS : (debugging purpose) History of computation,
%               niter - total iteration number spent for Nesterov's optimal
%               gradient method,
%               cpus - CPU seconds at iteration rounds,
%               objf - objective function values at iteration rounds,
%               prjg - projected gradient norm at iteration rounds.

% Note: another file 'GetStopCriterion.m' should be included under the same
% directory as this code.

% [m,n]=size(V);
% [~,r] = size(W_INIT);

% Default setting
MaxIter = MAX_ITER;
MinIter = 10;
MaxTime = 100000;
W0 = W_INIT;
H0 = H_INIT;
tol = TOL;

ITER_MAX = 1000;      % maximum inner iteration number (Default)
ITER_MIN = 10;        % minimum inner iteration number (Default)
global STOP_RULE;
STOP_RULE = 1;      % '1' for Projected gradient norm (Default)
                    % '2' for Normalized projected gradient norm
                    % '3' for Normalized KKT residual

% Initialization
W = W0;
H = H0;
HVt = H*(V)';
HHt = H*H';
WtV = W'*(V);
WtW = W'*W;
GradW = W * HHt - HVt';
GradH = WtW * H - WtV;
% init_delta = GetStopCriterion(STOP_RULE,[W',H],[GradW',GradH]);
init_delta = sqrt(norm(GradW,'fro')^2 + norm(GradH,'fro')^2);
tolH = max(tol,1e-3)*init_delta;
tolW = tolH;               % Stopping tolerance

constV = norm(V,'fro')^2;

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = sum(sum(WtW.*HHt))-2*sum(sum(WtV.*H));
HIS.prjg = init_delta;
HIS.relerr = norm(V - W*H,'fro') / norm(V, 'fro');

% Iterative updating
elapse = cputime;
W = W';
for iter = 1:MaxIter
    
    % Optimize H with W fixed
    [H,iterH] = NNLS(H,WtW,WtV,ITER_MIN,ITER_MAX,tolH);
    if iterH <= ITER_MIN
        tolH = tolH/10;
    end
    HHt = H*H';
    HVt = H*(V)';
    
    % Optimize W with H fixed
    [W,iterW,GradW] = NNLS(W,HHt,HVt,ITER_MIN,ITER_MAX,tolW);
    if iterW <= ITER_MIN
        tolW = tolW/10;
    end
    WtW = W*W';
    WtV = W*(V);
    GradH = WtW * H - WtV;
    
       
    HIS.niter = HIS.niter + iterH + iterW;
%     delta = GetStopCriterion(STOP_RULE,[W,H],[GradW,GradH]);
    delta = sqrt(  norm(myProjGrad(H, GradH, 0, 1e10), 'fro')^2 + ...
                    norm(myProjGrad(W, GradW, 0, 1e10), 'fro')^2);
    
    % Output running detials
    objf = sum(sum(WtW.*HHt)) - 2*sum(sum(WtV.*H));
    HIS.objf = [HIS.objf, objf];
    HIS.cpus = [HIS.cpus, cputime-elapse];
    HIS.prjg = [HIS.prjg, delta];
    relerr = norm(V - W'*H,'fro') / norm(V, 'fro');
    HIS.relerr = [HIS.relerr,relerr];
    
    if rem(iter,10) == 0
        fprintf('%d:\tstopping criteria = %e,\tobjective value = %f.\n', iter,delta/init_delta,HIS.objf(end)+constV);
    end
    
    % Stopping condition
    if (delta <= tol*init_delta && iter >= MinIter) || HIS.cpus(end) >= MaxTime
        break;
    end
end
W = W';
elapse = cputime - elapse;

HIS.objf = 0.5*(HIS.objf+constV);
fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);
return;

% Non-negative Least Squares with Nesterov's Optimal Gradient Method
function [H,iter,Grad] = NNLS(Z,WtW,WtV,iterMin,iterMax,tol)

global STOP_RULE;

if ~issparse(WtW)
    L = norm(WtW);	% Lipschitz constant
else
    L = norm(full(WtW));
end
H = Z;    % Initialization
Grad = WtW*Z - WtV;     % Gradient
alpha1 = 1;

for iter = 1:iterMax
    H0 = H;
    H = max(Z - Grad/L,0);    % Calculate sequence 'Y'
    alpha2 = 0.5*(1 + sqrt(1 + 4*alpha1^2));
    Z = H + ((alpha1-1)/alpha2)*(H-H0);
    alpha1 = alpha2;
    Grad = WtW * Z - WtV;
    
    % Stopping criteria
    if iter >= iterMin
        % Lin's stopping condition
        pgn = GetStopCriterion(STOP_RULE,Z,Grad);
        if pgn <= tol
            break;
        end
    end
end

Grad = WtW * H - WtV;

return;

function retVal=GetStopCriterion(stop_rule,X,gradX)
% Stopping Criterions
% Written by Naiyang (ny.guan@gmail.com)

switch stop_rule
    case 1
        pGrad=gradX(gradX<0|X>0);
        retVal=norm(pGrad);
    case 2
        pGrad=gradX(gradX<0|X>0);
        pGradNorm=norm(pGrad);
        retVal=pGradNorm/length(pGrad);
    case 3
        resmat=min(X,gradX); resvec=resmat(:);
        deltao=norm(resvec,1);  %L1-norm
        num_notconv=length(find(abs(resvec)>0));
        retVal=deltao/num_notconv;
end


function pgd = myProjGrad(x, gradx, lLim, uLim)
pgd = gradx .* ((x >= lLim) & (x <= uLim)) + min(0, gradx) .* (x <= lLim) + max(0, gradx) .* (x >= uLim);