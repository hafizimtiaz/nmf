function [W,H,R,iter, elapse, HIS] = nmf_myalg(V,Winit,Hinit,Rinit,lambda,tol,timelimit,maxiter)

% Code for non-negative Matrix Factorization with outliers 

% Reference
% Chih-Jen Lin. 2007. Projected Gradient Methods for Nonnegative Matrix 
% Factorization. Neural Comput. 19, 10 (October 2007), 2756-2779. 
% DOI=http://dx.doi.org/10.1162/neco.2007.19.10.2756

% Model: V = WH + R
% V (d x n): data matrix including n samples in d-dimensions
% W (d x k): basis matrix including k bases in d-dimensions
% H (k x n): coefficients matrix includeing n encodings in k-dimensions
% R (d x n): outlier matrix includeing n outlier vectors in d-dimensions

% Written by Hafiz Imtiaz (hafiz.imtiaz@rutgers.edu), based on the ALSPGRAD code by Chih-Jen Lin

% <Inputs>
%        V      : input data matrix (d x n)
%        Winit : (d x k) initial value for W.
%        Hinit : (k x n) initial value for H.
%        Rinit : (d x n) initial value for R.
%        lambda : regularizer for outliers.
%        tol    : stopping tolerance.
%        maxiter : maximum number of iterations.

% <Outputs>
%        W : estimated basis matrix (d x k).
%        H : estimated coefficients matrix (k x n).
%        R : estimated outlier matrix (d x n).
%        iter : number of top-level iterations.
%        elapse : CPU time in seconds.
%        HIS : history of computation,
%               niter - total iteration number spent for each sub-iteration
%               cpus - CPU seconds at iteration rounds,
%               objf - objective function values at iteration rounds,
%               prjg - projected gradient norm at iteration rounds.
%               relerr - relative error at each top-level iteration


W = Winit; 
H = Hinit; 
R = Rinit;
M = 1;

initt = cputime;
gradW = W*(H*H') - (V-R)*H';
gradH = (W'*W)*H - W'*(V-R);
gradR = R - (V - W*H) + lambda*sign(R);
initgrad = sqrt(norm(gradW, 'fro')^2 + ...
                norm(gradH, 'fro')^2 + ...
                norm(gradR, 'fro')^2);

fprintf('Init gradient norm %f\n', initgrad);

tolW = max(0.001,tol)*initgrad;
tolH = tolW;
tolR = 100*tol;

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = (1/2) * trace((V - W*H - R)'*(V - W*H - R)) + lambda * sum(sum(abs(R)));
HIS.prjg = initgrad;
HIS.relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');
elapse = cputime;

for iter = 1:maxiter
    
  % solving for W with H and R fixed
  [W, gradW, iterW] = nlssubprob(V',H',W', R', tolW, 1000);
  W = W'; 
  gradW = gradW';
  [~, K] = size(W);
  for k = 1:K
      W(:,k) = W(:,k) / max( 1 , norm(W(:,k)) );
  end
  
  if iterW < 2
      tolW = 0.1 * tolW;
  end
  
  % solving for H with W and R fixed
  [H, gradH, iterH] = nlssubprob(V, W, H, R, tolH, 1000);
  
  if iterH < 2
      tolH = 0.1 * tolH;
  end
  
  % solving for R with W and H fixed
  [R, gradR, iterR] = nlssubprobR(V, W, H, R, lambda, M, tolR, 1000);
  
  
  HIS.niter = HIS.niter + iterH + iterW + iterR;
  HIS.cpus = [HIS.cpus, cputime-elapse];
  
  % stopping condition
  projnorm = sqrt(  norm(myProjGrad(H, gradH, 0, 1e10), 'fro')^2 + ...
                    norm(myProjGrad(W, gradW, 0, 1e10), 'fro')^2 + ...
                    norm(myProjGrad(R, gradR, -M, M), 'fro')^2);
  
  % Output running detials
  objf = (1/2) * trace((V - W*H - R)'*(V - W*H - R)) + lambda * sum(sum(abs(R)));
  HIS.objf = [HIS.objf, objf];
  HIS.prjg = [HIS.prjg, projnorm];
  relerr = norm(V - W*H - R,'fro') / norm(V, 'fro');
  HIS.relerr = [HIS.relerr,relerr];
  
  if rem(iter,10) == 0
      fprintf('%d:\tstopping criteria = %e,\tobjective value = %f.\n', iter,projnorm/initgrad,HIS.objf(end));
  end
    
  if (projnorm < tol*initgrad) || (cputime-initt > timelimit)
      break
  end
  
end
elapse = cputime - elapse;

fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);


% helper functions: alternating non-negative least squares 
function [H,grad,iter] = nlssubprob(V,W,Hinit,R,tol,maxiter)
% H , grad = output solution and gradient
% iter = #iterations used
% V , W = constant matrices
% Hinit = initial solution
% R = outlier matrix
% tol = stopping tolerance
% maxiter = limit of iterations

H = Hinit; 
V = V - R;
WtV = W'*V;
WtW = W'*W;
alpha = 1; beta = 0.1;
for iter = 1:maxiter
  grad = WtW*H - WtV;
  
  projgrad = norm(grad(grad < 0 | H > 0));
  if projgrad < tol
      break
  end
  
  % search step size
  for inner_iter = 1:20
    Hn = max(H - alpha*grad, 0); 
    d = Hn-H;
    gradd = sum(sum(grad.*d)); 
    dQd = sum(sum((WtW*d).*d));
    suff_decr = 0.99*gradd + 0.5*dQd < 0;
    if inner_iter == 1
      decr_alpha = ~suff_decr; 
      Hp = H;
    end
    
    if decr_alpha
        if suff_decr
            H = Hn; 
            break;
        else
            alpha = alpha * beta;
        end
    else
        if (~suff_decr) | (Hp == Hn)
            H = Hp; 
            break;
        else
            alpha = alpha/beta; 
            Hp = Hn;
        end
    end
  end
end

if iter == maxiter
  fprintf('Max iter in nlssubprob\n');
end

function pgd = myProjGrad(x, gradx, lLim, uLim)
pgd = gradx .* ((x >= lLim) & (x <= uLim)) + min(0, gradx) .* (x <= lLim) + max(0, gradx) .* (x >= uLim);

function [R,grad,iter] = nlssubprobR(V,W,H,Rinit,lambda,M,tol,maxiter)
% R, grad = output solution and gradient
% iter = #iterations used
% V , W, H = constant matrices
% Rinit = initial solution
% tol = stopping tolerance
% M = algorithm parameter
% maxiter = limit of iterations

R = Rinit; 
objective_old = (1/2) * trace((V - W*H - R)'*(V - W*H - R)) + lambda * sum(sum(abs(R)));
iter = 0;
flag = true;

while (iter <= maxiter) && flag
    iter = iter + 1;
    
    R = mySoftThresh(V - W*H, lambda, M);
    
    % stopping condition check
    objective_new = (1/2) * trace((V - W*H - R)'*(V - W*H - R)) + lambda * sum(sum(abs(R)));
    if abs(objective_new - objective_old) < tol * objective_old
        flag = false;
    else
        objective_old = objective_new;
    end
end

grad = R - (V - W*H) + lambda*sign(R);

if iter==maxiter
  fprintf('Max iter in nlssubprobR\n');
end

function y = mySoftThresh(x, lambda, M)
    % Implementation of the soft thresholding function defined in Renbo
    % Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    
    % x - input vector of dimension F
    % lambda - penalty parameter (here threshold parameter)
    % M - parameter of the constraint set of the outlier r
    % y - output vector of dimension F
    
    y = 0 * (abs(x) < lambda) + (x - sign(x) * lambda) .* ((x >= lambda) & (x <= M)) + (sign(x) * M) .* (x >= lambda + M);
return