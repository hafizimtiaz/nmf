function [W, dist_H, dist_R, iter, elapse, HIS] = nmf_mydistneogm(dist_V, Winit, Hinit, Rinit, LAMBDA, TOL, TIMELIMIT, MAXITER)

% Code for distributed non-negative Matrix Factorization with outliers: NeNMF approach 

% Outputs:
% W - estimated global W
% dist_H - structure containing estimated H in different sites
% dist_R - structure containing estimated R in different sites
% iter - number of toplevel iteration (communication rounds)
% elapse - total elapseed time
% HIS - History of computation,
%       niter - total iteration number spent 
%       cpus - CPU seconds at iteration rounds,
%       objf - objective function values at iteration rounds,
%       prjg - projected gradient norm at iteration rounds.
%       relerr - ratio of frobenius norm of error and sample matrix

% Inputs:
% dist_V - structure containing samples in each site
% Winit, Hinit, Rinit - initial values
% lambda - regularizer parameter
% tol - parameter for stopping condition
% timelimit, maxiter - for stopping the iteration

% Written by Hafiz Imtiaz (hafiz.imtiaz@rutgers.edu), based on the ALSPGRAD code by Chih-Jen Lin

S = length(dist_V); % number of sites
MinIter = 10;
% MaxTime = 100000;

% initialization
W = Winit;
dist_H = cell(S,1);
dist_R = cell(S,1);
for s = 1:S
    dist_H{s} = Hinit;
    dist_R{s} = Rinit;
end

K = size(W,2);
lambda = LAMBDA;
tol = TOL;
maxiter = MAXITER;
timelimit = TIMELIMIT;

%% distributed NMF
flag = true;
iter = 0;
M = 1;

initgradW = 0;
initgradH = 0;
initgradR = 0;
objective_old = 0;
Vtest = [];
Htest = [];
Rtest = [];
for s = 1:S
    Gs = W * (dist_H{s} * dist_H{s}') - (dist_V{s} - dist_R{s}) * dist_H{s}';
    initgradW = initgradW + Gs;
    initgradH = initgradH + norm((W'*W)*dist_H{s} - W'*(dist_V{s} - dist_R{s}),'fro')^2;
    initgradR = initgradR + norm(dist_R{s} - (dist_V{s} - W * dist_H{s}) + lambda * sign(dist_R{s}),'fro')^2;
    objective_old = objective_old + (1/2)*norm(dist_V{s} - W * dist_H{s} - dist_R{s}, 'fro')^2 + lambda * sum(sum(abs(dist_R{s})));
    % the following quantities are for debugging
    Vtest = [Vtest dist_V{s}];
    Htest = [Htest dist_H{s}];
    Rtest = [Rtest dist_R{s}];
end

initgradW = norm(initgradW,'fro');
initgrad = sqrt(initgradW^2 + initgradH + initgradR);

tolW = max(0.001,tol)*initgrad;
tolH = tol*ones(S,1);
% tolR = tol*ones(S,1);

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = objective_old;
HIS.prjg = initgrad;
HIS.relerr = norm(Vtest - W*Htest - Rtest,'fro') / norm(Vtest, 'fro');
elapse = cputime;
initt = cputime;

while flag && (iter <= maxiter)
    iter = iter + 1;
    gradW_HHt = 0;
    gradW_VHt = 0;
    
    % update H and R in local sites
    Htest = [];
    Rtest = [];
    tot_iter_HR = 0;
    projgradH = 0;
    projgradR = 0;
    for s = 1:S
        % update H and R at local site
        [dist_H{s}, dist_R{s}, iterHR(s)] = myNNLS(dist_V{s}, W, dist_H{s}, dist_R{s}, lambda, MinIter, maxiter, tolH(s));
        if iterHR(s) < MinIter
            tolH(s) = 0.1 * tolH(s);
        end
        
        % sending these two information to central site 
        gradH = (W'*W) * dist_H{s} - W' * (dist_V{s} - dist_R{s});
        gradR = dist_R{s} - (dist_V{s} - W * dist_H{s}) + lambda * sign(dist_R{s});
        projgradH = projgradH + norm(myProjGrad(dist_H{s}, gradH, 0, 1e10), 'fro')^2; % scalar
        projgradR = projgradR + norm(myProjGrad(dist_R{s}, gradR, -M, M), 'fro')^2; % scalar
        gradW_HHt = gradW_HHt + dist_H{s} * dist_H{s}'; % k-by-k matrix
        gradW_VHt = gradW_VHt + (dist_V{s} - dist_R{s}) * dist_H{s}'; % d-by-k matrix
        
        % book keeping
        tot_iter_HR = tot_iter_HR + sum(iterHR);
        Htest = [Htest dist_H{s}];
        Rtest = [Rtest dist_R{s}];
    end
    
  
    % Optimize W at central site
    [W, iterW, gradW] = NNLS(W', gradW_HHt, gradW_VHt', MinIter, maxiter, tolW);
    W = W';
    gradW = gradW';
    
    for k = 1:K
        W(:,k) = W(:,k) / max( 1 , norm(W(:,k)) );
    end
    if iterW < MinIter
        tolW = tolW/10;
    end
    
    % Output running detials
    HIS.niter = HIS.niter + tot_iter_HR + iterW;
    HIS.cpus = [HIS.cpus, cputime-elapse];
    projnorm = sqrt(norm(gradW(gradW < 0 | W > 0))^2 + projgradH + projgradR);
    objf = (1/2) * norm(Vtest - W*Htest - Rtest,'fro')^2 + lambda * sum(sum(abs(Rtest)));
    HIS.objf = [HIS.objf, objf];
    HIS.prjg = [HIS.prjg, projnorm];
    relerr = norm(Vtest - W*Htest - Rtest,'fro') / norm(Vtest, 'fro');
    HIS.relerr = [HIS.relerr,relerr];
    
    if rem(iter,10) == 0
        fprintf('%d:\tstopping criteria = %e,\tobjective value = %f.\n', iter,projnorm/initgrad,HIS.objf(end));
    end
    
    % stopping condition check
    if iterW >= MinIter
        if (projnorm <= tol*initgrad) || (cputime-initt > timelimit)
            flag = false;
        end
    end
end
elapse = cputime - elapse;

fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);

%% helper functions
function [H,R,iter,Grad] = myNNLS(V,W,Z,R,lambda,iterMin,iterMax,tol)

WtW = W'*W;
if ~issparse(WtW)
    L = norm(WtW);	% Lipschitz constant
else
    L = norm(full(WtW));
end
H = Z;    % Initialization
WtV = W'*(V-R);
Grad = WtW*H - WtV;     % Gradient
GradR = R - (V - W*H) + lambda*sign(R);
initgrad = sqrt(norm(Grad, 'fro')^2 + norm(GradR, 'fro')^2);

alpha1 = 1; M = 1;

for iter = 1:iterMax
    % update H
    H0 = H;
    H = max(Z - Grad/L,0);    % Calculate sequence 'Y'
    alpha2 = 0.5*(1 + sqrt(1 + 4*alpha1^2));
    Z = H + ((alpha1-1)/alpha2)*(H-H0);
    alpha1 = alpha2;
    
    % update R
    R = mySoftThresh(V - W*H, lambda, 1);
    GradR = R - (V - W*H) + lambda*sign(R);
          
    WtV = W'*(V-R);
    Grad = WtW * Z - WtV;
    
    % Stopping criteria
    if iter >= iterMin
        % Lin's stopping condition
        projnorm = sqrt(  norm(myProjGrad(Z, Grad, 0, 1e10), 'fro')^2 + norm(myProjGrad(R, GradR, -M, M), 'fro')^2 );
        if (projnorm <= tol*initgrad) 
            break;
        end
    end
end

Grad = WtW * H - WtV;

return;

function y = mySoftThresh(x, lambda, M)
    % Implementation of the soft thresholding function defined in Renbo
    % Zhao et al. 2016 "Online NMF with Outliers", eqn 17
    
    % x - input vector of dimension F
    % lambda - penalty parameter (here threshold parameter)
    % M - parameter of the constraint set of the outlier r
    % y - output vector of dimension F
    
    y = 0 * (abs(x) < lambda) + (x - sign(x) * lambda) .* ((x >= lambda) & (x <= M)) + (sign(x) * M) .* (x >= lambda + M);
return

function [H,iter,Grad] = NNLS(Z,WtW,WtV,iterMin,iterMax,tol)


if ~issparse(WtW)
    L = norm(WtW);	% Lipschitz constant
else
    L = norm(full(WtW));
end
H = Z;    % Initialization
Grad = WtW*Z - WtV;     % Gradient
alpha1 = 1;

for iter = 1:iterMax
    H0 = H;
    H = max(Z - Grad/L,0);    % Calculate sequence 'Y'
    alpha2 = 0.5*(1 + sqrt(1 + 4*alpha1^2));
    Z = H + ((alpha1-1)/alpha2)*(H-H0);
    alpha1 = alpha2;
    Grad = WtW * Z - WtV;
    
    % Stopping criteria
    if iter >= iterMin
        % Lin's stopping condition
        pgn = norm(Grad(Grad < 0 | Z > 0));
        if pgn <= tol
            break;
        end
    end
end

Grad = WtW * H - WtV;

return;

function pgd = myProjGrad(x, gradx, lLim, uLim)
pgd = gradx .* ((x >= lLim) & (x <= uLim)) + min(0, gradx) .* (x <= lLim) + max(0, gradx) .* (x >= uLim);

