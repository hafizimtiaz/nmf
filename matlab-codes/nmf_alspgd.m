function [W,H, iter, elapse, HIS] = nmf_alspgd(V,Winit,Hinit,tol,timelimit,maxiter)

% NMF by alternative non-negative least squares using projected gradients

% Reference
% Chih-Jen Lin. 2007. Projected Gradient Methods for Nonnegative Matrix 
% Factorization. Neural Comput. 19, 10 (October 2007), 2756-2779. 
% DOI=http://dx.doi.org/10.1162/neco.2007.19.10.2756

% W , H = output solution
% Winit , Hinit = initial solution
% tol = tolerance for a relative stopping condition
% timelimit , maxiter = limit of time and iterations
% Edit: additional outputs by hafiz Imtiaz
%        HIS : (debugging purpose) History of computation,
%               niter - total iteration number spent for Nesterov's optimal
%               gradient method,
%               cpus - CPU seconds at iteration rounds,
%               objf - objective function values at iteration rounds,
%               prjg - projected gradient norm at iteration rounds.
%               relerr - relative error at each top-level iteration

W = Winit; 
H = Hinit; 
initt = cputime;
gradW = W*(H*H') - V*H';
gradH = (W'*W)*H - W'*V;
initgrad = norm([gradW; gradH'],'fro');

fprintf('Init gradient norm %f\n', initgrad);

tolW = max(0.001,tol)*initgrad;
tolH = tolW;

% Historical information
HIS.niter = 0;
HIS.cpus = 0;
HIS.objf = (1/2) * norm(V - W*H,'fro')^2;
HIS.prjg = initgrad;
HIS.relerr = norm(V - W*H,'fro') / norm(V, 'fro');
elapse = cputime;

for iter = 1:maxiter
  
  [W, gradW, iterW] = nlssubprob(V',H',W', tolW, 1000);
  W = W'; 
  gradW = gradW';
  
  if iterW == 1
      tolW = 0.1 * tolW;
  end
  
  [H, gradH, iterH] = nlssubprob(V, W, H, tolH, 1000);
  
  if iterH == 1
      tolH = 0.1 * tolH;
  end
  
  HIS.niter = HIS.niter + iterH + iterW;
  
  % stopping condition
  projnorm = norm([gradW(gradW < 0 | W > 0); gradH(gradH < 0 | H > 0)]);
  
  % Output running detials
  objf = (1/2) * norm(V - W*H,'fro')^2;
  HIS.objf = [HIS.objf, objf];
  HIS.cpus = [HIS.cpus, cputime-elapse];
  HIS.prjg = [HIS.prjg, projnorm];
  relerr = norm(V - W*H,'fro') / norm(V, 'fro');
  HIS.relerr = [HIS.relerr,relerr];
  
  if rem(iter,10) == 0
      fprintf('%d:\tstopping criteria = %e,\tobjective value = %f.\n', iter,projnorm/initgrad,HIS.objf(end));
  end
  
  if (projnorm < tol*initgrad) || (cputime-initt > timelimit)
      break
  end
end
elapse = cputime - elapse;

fprintf('\nFinal Iter = %d,\tFinal Elapse = %f.\n', iter,elapse);


% helper function: alternating non-negative least squares 
function [H,grad,iter] = nlssubprob(V,W,Hinit,tol,maxiter)
% H , grad = output solution and gradient
% iter = #iterations used
% V , W = constant matrices
% Hinit = initial solution
% tol = stopping tolerance
% maxiter = limit of iterations

H = Hinit; 
WtV = W'*V;
WtW = W'*W;
alpha = 1; beta = 0.1;
for iter = 1:maxiter
  grad = WtW*H - WtV;
  
  projgrad = norm(grad(grad < 0 | H >0));
  if projgrad < tol
      break
  end
  
  % search step size
  for inner_iter = 1:20
    Hn = max(H - alpha*grad, 0); 
    d = Hn-H;
    gradd = sum(sum(grad.*d)); 
    dQd = sum(sum((WtW*d).*d));
    suff_decr = 0.99*gradd + 0.5*dQd < 0;
    if inner_iter == 1
      decr_alpha = ~suff_decr; 
      Hp = H;
    end
    
    if decr_alpha
        if suff_decr
            H = Hn; 
            break;
        else
            alpha = alpha * beta;
        end
    else
        if (~suff_decr) | (Hp == Hn)
            H = Hp; 
            break;
        else
            alpha = alpha/beta; 
            Hp = Hn;
        end
    end
  end
end

if iter==maxiter
  fprintf('Max iter in nlssubprob\n');
end