# NMF
NMF is a MATLAB code collection for several non-negative matrix factorization algorithms. Currently it contains 8 functions:
* nmf_myalg - function for Proposed Algo1: NMF with outliers using alternating least squares approach
* nmf_myneogm - function for Proposed Algo2: NMF with outliers using Nesterov's optimal gradient approach
* nmf_mydistalg - function for Proposed distributed Algo1: distributed NMF with outliers using alternating least squares approach
* nmf_mydistneogm - function for Proposed distributed Algo2: distributed NMF with outliers using Nesterov's optimal gradient approach
* nmf_alspgd - function for NMF algorith due Chih-Jen Lin. 2007. Projected Gradient Methods for Nonnegative Matrix Factorization. Neural Comput. 19, 10 (October 2007), 2756-2779
* nmf_neogm - function for NMF algorithm due N. Guan, D. Tao, Z. Luo, and B. Yuan. NeNMF: An Optimal Gradient Method for Non-negative Matrix Factorization. IEEE Transactions on Signal Processing, Vol. 60, No. 6, PP. 2882-2898, Jun. 2012.
* nmf_opgd - function for online NMF algorithm due Zhao, Renbo; Tan, Vincent Y. F., 2016. Online Nonnegative Matrix Factorization With Outliers. IEEE Transactions on Signal Processing, vol. 65, issue 3, pp. 555-570
* nmf_bpgd - function for batch NMF algorithm due Zhao, Renbo; Tan, Vincent Y. F., 2016. Online Nonnegative Matrix Factorization With Outliers. IEEE Transactions on Signal Processing, vol. 65, issue 3, pp. 555-570

## Contact

* Hafiz Imtiaz (hafiz.imtiaz@rutgers.edu)

## Dependencies

The codes are tested on MATLAB R2016a. The helper functions are included in the function files.


## Downloading

You can download the repository at https://gitlab.com/hafizimtiaz/nmf.git, or using

```
$ git clone https://gitlab.com/hafizimtiaz/nmf.git
```

## Installation

Copy the functions in your working directiory or add the directory containing the functions in MATLAB path.


## License

MIT license

## Acknowledgements

This development of this collection was supported by support from the
following sources:

* National Institutes of Health under award 1R01DA040487-01A1

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of National Institutes of Health.
